# Introduction
This is a very simple tool that finds a list of peers (IP and Port) for every
tracker of a torrent and returns it at JSON.

I had to gather some statistics about movie trends. I decided to get them from
torrents so I build this tool to list all the IPs which I then can look up in
a GeoIp database and aggregate the data.

# Usage
At the moment I have not yet published to crates.io because I want to implement
several more features before that. You just have to clone the repository and
build the crate.

## Example usage
I will use the debian iso magnet link at the time of this writing; be aware of
that in case the command does not produce the same output. *jq* is just used
for formatting.
```sh
$ cargo run -- --magnet $(cat debian_magnet.txt) | jq
{
  "name": "debian-10.5.0-i386-netinst.iso",
  "infohash": "ed393f460680eb1456b62da81b0bc65e50cc2488",
  "trackers": [
    {
      "tracker": "http://bttracker.debian.org:6969/announce",
      "peers": [
        {
          "ip": "5.105.175.112",
          "port": 197
        },
        {
          "ip": "87.188.147.169",
          "port": 54728
        },
        {
          "ip": "66.222.253.43",
          "port": 14386
        },
        ...
        {
          "ip": "69.195.158.163",
          "port": 47346
        },
        {
          "ip": "92.111.172.106",
          "port": 21275
        }
      ],
      "interval": 900
    }
  ]
}
```
In order to see all the options run:
```sh
$ cargo run -- --help
TorrentStats 0.1
Gathers a list of peers for every tracker in a torrent.

USAGE:
    torrentstats [OPTIONS] <--magnet <LINK>>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -m, --magnet <LINK>    Magnet link
```

# Documentation
Until we publish to *crates.io* and *doc.rs* you can generate the documentation
and explore it locally:
```sh
$ cargo doc --no-deps --open
```

# Future features
## Features
- [x] Magnet links
- [ ] Torrent files
- [ ] Add approximate location of the IP by a CLI option.
## Code
- [ ] Tests
