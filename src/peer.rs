use std::convert::TryInto;
use std::net::Ipv4Addr;

use serde::Serialize;

/// Single peer for a tracker.
#[derive(Debug, Serialize)]
pub struct Peer {
    pub ip: Ipv4Addr,
    pub port: u16,
}
/// Deserializes an array of byte into an array of peers. The input bytes have
/// to be in compact peer format (4 bytes for IP and 2 bytes for Port concatenated).
pub fn deserialize_peers(s: &[u8]) -> Vec<Peer> {
    let mut result = Vec::new();
    for i in (0..s.len()).step_by(6) {
        let ip_bytes: [u8; 4] = s[i..i + 4].try_into().expect("Array with incorrect length");
        let ip = Ipv4Addr::from(ip_bytes);
        let port: u16 = u16::from_ne_bytes(
            s[i + 4..i + 6]
                .try_into()
                .expect("Array with incorrect length"),
        );
        result.push(Peer { ip, port });
    }
    return result;
}
