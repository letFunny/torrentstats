use std::convert::TryFrom;

use serde::Serialize;

use crate::data::DataPeer;
use crate::error::TSError;

pub mod http;
pub mod udp;

/// UDP or HTTP tracker.
#[derive(Debug, Clone, Serialize)]
#[serde(untagged)]
pub enum Tracker {
    // TODO http::Uri instead of String
    UDP(String),
    HTTP(String),
}
impl TryFrom<&str> for Tracker {
    type Error = TSError;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if &value[..6] == "udp://" {
            Ok(Tracker::UDP(value.to_string()))
        } else if &value[..7] == "http://" {
            Ok(Tracker::HTTP(value.to_string()))
        } else {
            Err(TSError::InvalidTrackerProtocol(value.to_string()))
        }
    }
}
/// Unified response from a tracker (either HTTP or UDP) that contains all the
/// fields that were returned. Note that the response may seeders and leechers
/// in the case of a HTTP tracker.
#[derive(Debug)]
pub struct TrackerFinalResponse {
    pub peers: Vec<DataPeer>,
    pub interval: i64,
    pub seeders: Option<i32>,
    pub leechers: Option<i32>,
}
