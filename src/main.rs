use clap::{App, Arg, ArgGroup};

use torrentstats::error::TSError;
use torrentstats::torrent::Torrent;
use torrentstats::TorrentStats;

fn main() -> Result<(), TSError> {
    // If no variable is set in the environment, set the loggin level to warn.
    env_logger::from_env(env_logger::Env::default().default_filter_or("warn")).init();
    let matches = App::new("TorrentStats")
        .version("0.1")
        .about("Gathers a list of peers for every tracker in a torrent.")
        .arg(
            Arg::with_name("magnet")
                .short("m")
                .long("magnet")
                .value_name("LINK")
                .help("Magnet link")
                .takes_value(true),
        )
        //.arg(
        //    Arg::with_name("torrent")
        //        .short("t")
        //        .long("torrent")
        //        .value_name("FILE")
        //        .help("Name of the torrent file")
        //        .takes_value(true),
        //)
        .group(
            ArgGroup::with_name("command")
                //.args(&["torrent", "magnet"])
                .args(&["magnet"])
                .required(true),
        )
        .get_matches();
    let torrent = if let Some(magnet_name) = matches.value_of("magnet") {
        Torrent::from_magnet(magnet_name)?
    }
    /*else if let Some(torrent_path) = matches.value_of("torrent") {
        Torrent::from_torrent_file(torrent_path)?
    } */
    else {
        panic!("Should not reach here with given CLI options");
    };

    let mut app = TorrentStats::new();
    let data = app.torrent_stats(&torrent);
    println!("{}", serde_json::to_string(&data).unwrap());
    Ok(())
}
