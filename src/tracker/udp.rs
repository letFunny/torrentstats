use std::convert::TryInto;
use std::net::{SocketAddr, ToSocketAddrs};
use tokio::net::UdpSocket;

use rand::Rng;

use crate::data::DataPeer;
use crate::error::TSError;
use crate::peer::deserialize_peers;
use crate::torrent::InfoHash;
use crate::tracker::TrackerFinalResponse;

/// Given a tracker URL and a InfoHash it connects to the tracker following the
/// UDP tracker specification and asks for a list of peers if the different
/// message exchanging were successful.
pub async fn get_peer_list_udp(
    url: &str,
    info_hash: &InfoHash,
) -> Result<TrackerFinalResponse, TSError> {
    // We have to remove the trailing announce because the rust std complains
    // in the lookup if the url is not <host>:<port>.
    // We also remove udp:// which is always present (not like /announce) so
    // we can use a range [6..]
    let clean_url = url[6..].replace("/announce", "");
    // Lookup the hostname and get the IPv4 so that the socket does not
    // complain if it resolved it itself and got the IPv6 version.
    let addr = clean_url
        .to_socket_addrs()?
        .find(|u| match u {
            SocketAddr::V4(_) => true,
            _ => false,
        })
        .ok_or(TSError::InvalidHostName(url.to_string()))?;

    // Bind to localhost and random available port
    let mut socket = UdpSocket::bind("0.0.0.0:0").await?;
    // TODO move to byte container from bytes crate
    // TODO Use vec with capacity rust
    let mut connect = Vec::new();
    // connection_id
    connect.extend_from_slice(&0x41727101980i64.to_be_bytes());
    // action
    connect.extend_from_slice(&0i32.to_be_bytes());
    // transaction_id
    let my_transaction_id_bytes = rand::thread_rng().gen::<[u8; 4]>();
    connect.extend_from_slice(&my_transaction_id_bytes);

    // TODO for some reason if we connect, we get no route to host (os error 113)
    // For now we are going to send_to and recv_from instead.
    // socket.connect(addr).await?;
    // socket.send(&connect).await?;
    socket.send_to(&connect, addr).await?;
    let mut buf = vec![0u8; 1024];
    if 16 != socket.recv_from(&mut buf).await?.0 {
        return Err(TSError::InvalidResponse);
    }
    let action = i32::from_be_bytes(
        (&buf[0..4])
            .try_into()
            .expect("Array with incorrect length"),
    );
    if 0 != action {
        return Err(TSError::InvalidResponse);
    }
    let transaction_id_bytes: [u8; 4] = (&buf[4..8])
        .try_into()
        .expect("Array with incorrect length");
    if my_transaction_id_bytes != transaction_id_bytes {
        return Err(TSError::InvalidResponse);
    }
    let connection_id_bytes: [u8; 8] = (&buf[8..16])
        .try_into()
        .expect("Array with incorrect length");

    let mut announce = Vec::new();
    // connection_id
    announce.extend_from_slice(&connection_id_bytes);
    // action
    announce.extend_from_slice(&1i32.to_be_bytes());
    // transaction_id
    announce.extend_from_slice(&my_transaction_id_bytes);
    // info_hash
    announce.extend_from_slice(&info_hash);
    // peer_id
    let peer_id = rand::thread_rng().gen::<[u8; 20]>();
    announce.extend_from_slice(&peer_id);
    // downloaded
    announce.extend_from_slice(&0i64.to_be_bytes());
    // left
    announce.extend_from_slice(&(-1i64).to_be_bytes());
    // uploaded
    announce.extend_from_slice(&0i64.to_be_bytes());
    // event: 2 = started
    announce.extend_from_slice(&2i32.to_be_bytes());
    // IP address
    announce.extend_from_slice(&0i32.to_be_bytes());
    // key
    announce.extend_from_slice(&0i32.to_be_bytes());
    // num_want
    announce.extend_from_slice(&(-1i32).to_be_bytes());
    // port
    announce.extend_from_slice(&6881i16.to_be_bytes());
    socket.send_to(&announce, addr).await?;
    let length = socket.recv_from(&mut buf).await?.0;
    if 20 > length || (length - 20) % 6 != 0 {
        return Err(TSError::InvalidResponse);
    }
    let interval = i32::from_be_bytes(
        (&buf[8..12])
            .try_into()
            .expect("Array with incorrect length"),
    ) as i64;
    let seeders = Some(i32::from_be_bytes(
        (&buf[12..16])
            .try_into()
            .expect("Array with incorrect length"),
    ));
    let leechers = Some(i32::from_be_bytes(
        (&buf[16..20])
            .try_into()
            .expect("Array with incorrect length"),
    ));
    // Gets the peers from compact representation (4 bytes IP + 2 bytes Port).
    // See the function for more information.
    let peers: Vec<_> = deserialize_peers(&buf[20..length])
        .into_iter()
        .map(|peer| DataPeer {
            peer,
            location: None,
        })
        .collect();
    Ok(TrackerFinalResponse {
        peers,
        interval,
        seeders,
        leechers,
    })
}
