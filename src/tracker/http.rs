use percent_encoding::{percent_encode, NON_ALPHANUMERIC};
use rand::Rng;
use reqwest;
use serde::{Deserialize, Deserializer};
use serde_bytes::ByteBuf;

use crate::data::DataPeer;
use crate::error::TSError;
use crate::peer::{deserialize_peers, Peer};
use crate::torrent::InfoHash;
use crate::tracker::TrackerFinalResponse;

/// Helper to translate the peers from compact mode (4 bytes IP + 2 bytes port)
/// to a proper IP and Port.
fn serde_deserialize_peers<'de, D>(deserializer: D) -> Result<Vec<Peer>, D::Error>
where
    D: Deserializer<'de>,
{
    let s: ByteBuf = Deserialize::deserialize(deserializer)?;
    return Ok(deserialize_peers(&s));
}
/// The response we receive when connecting with a HTTP tracker.
#[derive(Debug, Deserialize)]
pub struct HttpTrackerResponse {
    interval: i64,
    #[serde(deserialize_with = "serde_deserialize_peers")]
    peers: Vec<Peer>,
    peers6: ByteBuf,
}
/// Given a HTTP tracker url and a torrent InfoHash it asks for a list of peers
/// and parses it.
pub async fn get_peer_list_http(
    url: &str,
    info_hash: &InfoHash,
) -> Result<TrackerFinalResponse, TSError> {
    let client = reqwest::Client::new();
    let mut final_url = url.to_string();
    // TODO use serde as URL encoding
    let info_hash_str = percent_encode(&info_hash, NON_ALPHANUMERIC).to_string();
    // Generate random peer id so that all the requests dont look as coming from
    // the same source.
    let peer_id = rand::thread_rng().gen::<[u8; 20]>();
    let peer_id_str = percent_encode(&peer_id, NON_ALPHANUMERIC).to_string();
    final_url.push_str(&format!(
        "?info_hash={}&peer_id={}&port=6881&upload=0&downloaded=0&compact=1",
        info_hash_str, peer_id_str
    ));
    // Send the request.
    let res = client.get(&final_url).send().await?.bytes().await?;
    // Parse the response.
    let response = serde_bencode::de::from_bytes::<HttpTrackerResponse>(&res)
        .map_err(|source| TSError::InvalidBencode { source })?;
    let peers: Vec<DataPeer> = response
        .peers
        .into_iter()
        .map(|p| DataPeer {
            peer: p,
            location: None,
        })
        .collect();
    // We are not given seeders or leechers for HTTP trackers.
    let seeders = None;
    let leechers = None;
    Ok(TrackerFinalResponse {
        peers,
        interval: response.interval,
        seeders,
        leechers,
    })
}
