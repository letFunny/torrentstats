use std::convert::TryFrom;

use hex;
use percent_encoding::percent_decode_str;

use crate::error::TSError;
use crate::tracker::Tracker;

pub type InfoHash = Vec<u8>;
/// Represents a general "torrent" in the sense that it encompases all the
/// information needed to perform queries on the trackers. It can be generated
/// from a magnet link or a torrent file directly.
#[derive(Debug)]
pub struct Torrent {
    pub name: String,
    pub infohash: InfoHash,
    pub trackers: Vec<Tracker>,
}
//#[derive(Debug, Deserialize)]
//struct TorrentFile {
//    announce: String,
//    #[serde(skip_deserializing)]
//    info: ByteBuf,
//}

/// Simple function to decode the infohash which comes hex encoded in the
/// magnet link.
fn parse_infohash(s: &str) -> Result<InfoHash, TSError> {
    Ok(hex::decode(s).map_err(|_| TSError::InvalidMagnetLink)?)
}
impl Torrent {
    /// Gathers all the information for a torrent from a magnet link.
    pub fn from_magnet(link: &str) -> Result<Self, TSError> {
        // Remove the magnet:? string
        let link = *link
            .split("?")
            .collect::<Vec<&str>>()
            .get(1)
            .ok_or(TSError::InvalidMagnetLink)?;
        // Decode the string from url encoding
        let link_decoded = percent_decode_str(link)
            .decode_utf8()
            .map_err(|_| TSError::InvalidMagnetLink)?;
        // Split into the different uri components
        let tags: Vec<&str> = link_decoded.split("&").collect();

        // At least have a name, infohash and trackerlist
        let mut name = None;
        let mut infohash = None;
        let mut trackers = Vec::new();
        for &t in &tags {
            match &(t[..3]) {
                "dn=" => {
                    name = Some(t[3..].to_string());
                }
                "xt=" => {
                    infohash = Some(parse_infohash(&t[(3 + 9)..])?);
                }
                "tr=" => {
                    trackers.push(Tracker::try_from(&t[3..])?);
                }
                &_ => (),
            }
        }
        Ok(Torrent {
            name: name.ok_or(TSError::InvalidMagnetLink)?,
            infohash: infohash.ok_or(TSError::InvalidMagnetLink)?,
            trackers,
        })
    }
    //pub fn from_torrent_file<P: AsRef<Path>>(path: P) -> Result<Torrent, TSError> {
    //    let contents = read(path)?;
    //    let torrent_decoded: TorrentFile = serde_bencode::de::from_bytes(&contents)
    //        .map_err(|source| TSError::InvalidBencode { source })?;
    //    println!("{:#?}", torrent_decoded);
    //    Err(TSError::InvalidMagnetLink)
    //}
}
