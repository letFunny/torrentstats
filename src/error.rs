use thiserror::Error;

/// All the errors that the library can throw are shown here. Names and fmts
/// are self explanatory.
#[derive(Debug, Error)]
pub enum TSError {
    #[error("The tracker protocol `{0}` is not supported")]
    InvalidTrackerProtocol(String),
    #[error("Could not resolve `{0}`")]
    InvalidHostName(String),
    // TODO add further error on missing fields
    #[error("The magnet link is invalid")]
    InvalidMagnetLink,
    #[error("An error ocurred in one of the network requests")]
    RequestError {
        #[from]
        source: reqwest::Error,
    },
    #[error("The tracker responded with an invalid message")]
    InvalidResponse,
    #[error("The tracker response could not be parsed to bencode")]
    InvalidBencode { source: serde_bencode::error::Error },
    #[error(transparent)]
    IO(#[from] std::io::Error),
}
