use std::time::Duration;

use futures::future::join_all;
use hex;
use log::{error, warn};
use serde::{Serialize, Serializer};
use tokio::time::timeout;

use crate::peer::Peer;
use crate::torrent::{InfoHash, Torrent};
use crate::tracker::http::get_peer_list_http;
use crate::tracker::udp::get_peer_list_udp;
use crate::tracker::{Tracker, TrackerFinalResponse};

/// Maximum time (in miliseconds) that we wait for a response from a Tracker. The trackers are very
/// erratic so increasing this time can maybe help with that.
pub const TIMEOUT: u64 = 5000;

fn serde_serialize_infohash<S>(info_hash: &InfoHash, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let info_hash_str = hex::encode(info_hash);
    s.serialize_str(&info_hash_str)
}
/// Final data representation for a Torrent that encapsules all the data we have
/// about it.
#[derive(Debug, Serialize)]
pub struct DataTorrent {
    pub name: String,
    #[serde(serialize_with = "serde_serialize_infohash")]
    pub infohash: InfoHash,
    pub trackers: Vec<DataTracker>,
}
impl DataTorrent {
    /// Gets a torrent and a runtime to run all the different network connections
    /// to the trackers and get all the stats about the peers.
    pub fn new(runtime: &mut tokio::runtime::Runtime, torrent: &Torrent) -> DataTorrent {
        let res = runtime.block_on(async move {
            let futures = torrent
                .trackers
                .iter()
                // We enclose each future in a timeout of TIMEOUT seconds for when the
                // tracker does not respond or things are really slow. This is
                // far easier (less granular) than timeouts for every network
                // connection.
                .map(|t| {
                    timeout(
                        Duration::from_millis(TIMEOUT),
                        DataTracker::new(t, &torrent.infohash),
                    )
                })
                .collect::<Vec<_>>();

            // Filter the errors and get the trackers that return Ok
            let data_trackers: Vec<_> = join_all(futures)
                .await
                .into_iter()
                .filter_map(|dt| match dt {
                    Ok(dt) => Some(dt),
                    Err(_) => {
                        warn!("Reached timeout for tracker.");
                        None
                    }
                })
                .collect();
            return data_trackers;
        });
        DataTorrent {
            name: torrent.name.clone(),
            infohash: torrent.infohash.clone(),
            trackers: res,
        }
    }
}
/// Final data representation for a Tracker that encapsules all the data we have
/// about it including seeders/leechers depending on the type of tracker.
#[derive(Debug, Serialize)]
pub struct DataTracker {
    pub tracker: Tracker,
    pub peers: Vec<DataPeer>,
    pub interval: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub seeders: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub leechers: Option<i32>,
}
impl DataTracker {
    /// Last function in the data hierarchy. It gets the peers and the rest of
    /// the data and returns the proper structs.
    async fn new(tracker: &Tracker, info_hash: &InfoHash) -> Self {
        let TrackerFinalResponse {
            peers,
            interval,
            seeders,
            leechers,
        } = match tracker {
            Tracker::HTTP(url) => match get_peer_list_http(&url, info_hash).await {
                Ok(p) => p,
                Err(e) => {
                    error!("Could not get peers for '{}': {}", url, e);
                    TrackerFinalResponse {
                        peers: Vec::new(),
                        interval: 0,
                        seeders: None,
                        leechers: None,
                    }
                }
            },
            Tracker::UDP(url) => match get_peer_list_udp(&url, info_hash).await {
                Ok(p) => p,
                Err(e) => {
                    error!("Could not get peers for '{}': {}", url, e);
                    TrackerFinalResponse {
                        peers: Vec::new(),
                        interval: 0,
                        seeders: None,
                        leechers: None,
                    }
                }
            },
        };
        Self {
            tracker: tracker.clone(),
            peers,
            seeders,
            leechers,
            interval,
        }
    }
}
/// Convenience class for Peer. It is not that we get more information but that
/// we can encapsule more fields if needed in the last representation. For example
/// we could add a Location (based on IP).
#[derive(Debug, Serialize)]
pub struct DataPeer {
    #[serde(flatten)]
    pub peer: Peer,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location: Option<DataLocation>,
}
/// Location for a peer.
pub type DataLocation = String;
