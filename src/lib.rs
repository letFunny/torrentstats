use tokio;
use tokio::runtime;

use data::DataTorrent;

use torrent::Torrent;

pub mod data;
pub mod error;
pub mod peer;
pub mod torrent;
pub mod tracker;

/// Main struct that holds the state of the program: mainly the runtime.
pub struct TorrentStats {
    runtime: tokio::runtime::Runtime,
}
impl TorrentStats {
    /// Create a new runtime and state. This function only needs to be called
    /// once and reutilize the object for every torrent.
    pub fn new() -> Self {
        let runtime = runtime::Builder::new()
            .basic_scheduler()
            .enable_all()
            .build()
            .unwrap();
        TorrentStats { runtime }
    }
    /// Entry point to the library. Gather all the peers and stats from a torrent
    /// and return the final structured information.
    pub fn torrent_stats(&mut self, torrent: &Torrent) -> DataTorrent {
        DataTorrent::new(&mut self.runtime, &torrent)
    }
}
